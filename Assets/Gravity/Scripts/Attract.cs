﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//TO DO 
public class Attract : MonoBehaviour
{
    public bool isHided;
    public float attractionForce;
    public float attractRadius;
    [SerializeField]
    public List<Attract> affectedAttractedBalls = new List<Attract>();

    void FixedUpdate()
    {
        foreach (Collider collider in Physics.OverlapSphere(transform.position, attractRadius))
        {
            Vector3 forceDierction = transform.position - collider.transform.position;

            collider.GetComponent<Rigidbody>().AddForce(forceDierction.normalized * attractionForce * Random.RandomRange(1, 10) * Time.deltaTime);
        }

        if (affectedAttractedBalls.Count >= 49)
        {

        }
    }

    void OnCollisionEnter(Collision collision)
    {
        affectedAttractedBalls.Add(collision.gameObject.GetComponent<Attract>());
        float m = collision.collider.GetComponent<Rigidbody>().mass;
        GetComponent<Rigidbody>().mass += m;

        attractRadius += collision.collider.GetComponent<Attract>().attractRadius;
        transform.localScale = transform.localScale * collision.collider.GetComponent<Attract>().affectedAttractedBalls.Count;

        if (collision.gameObject.GetComponent<Attract>().attractRadius > this.attractRadius || !collision.gameObject.GetComponent<Attract>().isHided)
        {
            isHided = true;
            gameObject.SetActive(false);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, attractRadius);
    }
}
