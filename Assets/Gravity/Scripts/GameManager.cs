﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public GameObject prefab;
    public Text ballCounterText;
    public int ballCounter;

    private bool _canCreate;

    void Start()
    {
        _canCreate = true;
        StartCoroutine(InstantiateBall());
    }

    IEnumerator InstantiateBall()
    {
        while (ballCounter < 250)
        {
            yield return new WaitForSeconds(.25f);
            Vector3 pos = SetPositionInCamerView();

            Instantiate(prefab, pos, Quaternion.identity);
            UpdateUI();
        }
    }

    void UpdateUI()
    {
        ballCounter++;
        ballCounterText.text = "Crated balls = " +ballCounter.ToString();
    }

    private Vector3 SetPositionInCamerView()
    {
        float x = Random.Range(0.05f, 0.95f);
        float y = Random.Range(0.05f, 0.95f);
        float z = Random.Range(10f, 15f);
        Vector3 pos = new Vector3(x, y, z);
        pos = Camera.main.ViewportToWorldPoint(pos);
        return pos;
    }
}
