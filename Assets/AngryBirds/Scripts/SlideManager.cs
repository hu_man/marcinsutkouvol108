﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideManager : MonoBehaviour
{

    public GameObject ballPrefab;
    public Transform startingTransform;
    public CameraFollow cameraFollow;

    private Drag _currentDragObject;
    private GameObject _currentThrowedObject;

    void Start()
    {
        InstantiateObject();
    }

    void InstantiateObject()
    {
        GameObject go = Instantiate(ballPrefab,startingTransform.position, Quaternion.identity);
        _currentThrowedObject = go;
        _currentDragObject = go.GetComponent<Drag>();

        AddEvent();
    }

    void AddEvent()
    {
        _currentDragObject.OnGround += PreperNextObject;
        _currentDragObject.OnFired += StartCameraFollow;
    }

    void RemoveEvent()
    {
        _currentDragObject.OnGround -= PreperNextObject;
        _currentDragObject.OnFired -= StartCameraFollow;
    }

    void PreperNextObject()
    {
        StartCoroutine(WaitOneSecond());
        RemoveEvent();
    }

    IEnumerator WaitOneSecond()
    {
        yield return new WaitForSeconds(1);
        cameraFollow.ResetTarget();
        cameraFollow.ResetToStartPosition();
        InstantiateObject();
    }

   void StartCameraFollow()
    {
        cameraFollow.Init(_currentThrowedObject.transform);
    }
}
