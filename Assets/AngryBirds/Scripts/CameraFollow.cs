﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform _target;
    private Vector3 _offset;
    private Vector3 _startPosition;

    public void Init(Transform target)
    {
        _target = target;
        _startPosition = transform.position;
        _offset = transform.position - _target.position;
    }

    public void ResetTarget()
    {
        _target = null;
    }
    public void ResetToStartPosition()
    {
        transform.position = _startPosition;
    }

    void Update()
    {
        if (_target == null)
            return;

        transform.position = _target.position + _offset;
    }
}
