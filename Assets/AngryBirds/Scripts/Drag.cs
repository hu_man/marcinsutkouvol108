﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag : MonoBehaviour
{
    public float force;

    public delegate void ObjectGonGorund();
    public event ObjectGonGorund OnGround;
    public delegate void ObjectFired();
    public event ObjectFired OnFired;

    private Vector3 _screenPoint;
    private Vector3 _offset;
    private Vector3 _startPosition;
    private Vector3 _distance;
    private Rigidbody _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _startPosition = transform.position;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Ground"))
        {
            if (OnGround != null)
                OnGround.Invoke();
        }
    }

    void SetupBall(Transform throwObjectTransform, Rigidbody throwObjectRigidbody)
    {
        _rigidbody = throwObjectRigidbody;
    }

    void OnMouseDown()
    {
        _screenPoint = Camera.main.WorldToScreenPoint(_startPosition);
        _offset = _startPosition - Camera.main.ScreenToWorldPoint(
        new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset;
        transform.position = curPosition;

        _distance = _startPosition - curPosition;
        transform.position = new Vector3(_startPosition.x, curPosition.y, curPosition.z);
    }

    void OnMouseUp()
    {
        Throw();
        if (OnFired != null)
            OnFired.Invoke();
    }

    void Throw()
    {
        _rigidbody.isKinematic = false;
        Vector3 direction = new Vector3(0, _distance.y, _distance.z);
        _rigidbody.AddForce(direction * force, ForceMode.Impulse);
    }
}
